package cql.ecci.ucr.ac.cr.ejemplo21;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class LazyAdapter extends BaseAdapter {

    static final String KEY_NAME = "name";
    static final String KEY_IMAGE  = "image";
    static final String KEY_DSC  = "description";

    private ArrayList<HashMap<String, String>> mData;
    private Context mContext;

    public LazyAdapter(ArrayList<HashMap<String, String>> data, Context context){
        mData = data;
        mContext = context;
    }

    public int getCount(){
        return mData.size();
    }

    public Object getItem(int position){
        return position;
    }

    public long getItemId(int position){
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = null;

        HashMap<String, String> lista = new HashMap<String, String>();
        lista = mData.get(position);
        rowView = inflater.inflate(R.layout.list_row, parent, false);

        TextView nombre = rowView.findViewById(R.id.name);
        ImageView imagen = rowView.findViewById(R.id.icon);
        TextView descripcion = rowView.findViewById(R.id.description);

        nombre.setText(lista.get(KEY_NAME));
        imagen.setImageResource(Integer.parseInt(lista.get(KEY_IMAGE)));
        descripcion.setText(lista.get(KEY_DSC));

        return rowView;
    }
}

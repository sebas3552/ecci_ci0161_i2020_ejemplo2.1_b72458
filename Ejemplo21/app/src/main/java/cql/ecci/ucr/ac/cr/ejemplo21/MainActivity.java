package cql.ecci.ucr.ac.cr.ejemplo21;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView mList;
    private List<Tip> mTips;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mList = findViewById(R.id.list);

        mTips = new ArrayList<Tip>();

        mTips.add(new Tip("Agua", "01", "Al menos 8 vasos al día"));
        mTips.add(new Tip("Vino", "02", "No exceda una copa al día"));
        mTips.add(new Tip("Café", "03", "Evite tomarlo"));
        mTips.add(new Tip("Carnes", "04", "Al menos tres veces a la semana"));
        mTips.add(new Tip("Hamburguesas", "05", "Solo caseras y bajas en grasa"));

        ArrayList<HashMap<String, String>> mTipList = new ArrayList<HashMap<String, String>>();

        Iterator<Tip> iterator = mTips.iterator();
        while(iterator.hasNext()){
            Tip tip = iterator.next();
            HashMap<String, String> mMap = new HashMap<String, String>();
            mMap.put(LazyAdapter.KEY_NAME, tip.getName());
            mMap.put(LazyAdapter.KEY_DSC, tip.getDescription());
            int id = this.getResources().getIdentifier(Tip.IMAGEN + tip.getImg(), "drawable", this.getPackageName());
            mMap.put(LazyAdapter.KEY_IMAGE, Integer.toString(id));

            mTipList.add(mMap);
        }

        LazyAdapter mAdapter = new LazyAdapter(mTipList, this);

        mList.setAdapter(mAdapter);

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                String selectedItem = mTips.get(position).getName();
                Toast.makeText(getApplicationContext(), selectedItem, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
